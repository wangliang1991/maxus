package com.codingapi.maxus.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by lorne on 2018/3/14
 */
@RestController
public class HelloController {


    @RequestMapping("/hello")
    public String hello(){
        return "hello, i'm server.";
    }

    @RequestMapping(value = "/call",method = RequestMethod.GET)
    public String call(@RequestParam("name") String name){
        return "res--> hello:"+name;
    }
}
