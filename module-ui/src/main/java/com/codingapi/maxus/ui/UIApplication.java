package com.codingapi.maxus.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * create by lorne on 2018/3/14
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class UIApplication {

    public static void main(String[] args) {
        SpringApplication.run(UIApplication.class,args);
    }
}
