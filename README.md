## 启动说明

1. 首先启动consul服务发现。

2. 启动config-server配置中心，配置中心下的文件在git上。需要调整需要先调整git代码

3. 启动Client Server ui 服务

4. 访问http://127.0.0.1:8090/ 点击按钮测试。


## 调用关系说明

client - > server

ui -> client|server 

ui 作为界面展示层 html直接放在了static下，由于访问其他模块的限制跨越限制，采用了zuul方式访问。该方案可根据项目情况调整。

## config git

https://gitee.com/wangliang1991/config