package com.codingapi.maxus.client.controller;

import com.codingapi.maxus.client.feign.ServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by lorne on 2018/3/14
 */
@RestController
public class HelloController {

    @Autowired
    private ServerClient serverClient;

    @RequestMapping("/hello")
    public String hello(){
        return "hello, i'm client.";
    }

    @RequestMapping("/call")
    public String call(){
        return serverClient.call(" client");
    }
}
