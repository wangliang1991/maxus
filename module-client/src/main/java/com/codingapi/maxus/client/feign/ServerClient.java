package com.codingapi.maxus.client.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * create by lorne on 2018/1/26
 */
@FeignClient(value = "server",fallback = ServerClientHystrix.class)
public interface ServerClient {

    @RequestMapping(value = "/call",method = RequestMethod.GET)
    String call(@RequestParam("name") String name);

}
